package br.unisul.aula.banco;

import  javax.persistence.*;

public class JPAUtil {
    private static EntityManagerFactory entityManagerFactory;

    public static EntityManager getEntityManager(){
            if (entityManagerFactory==null){
             entityManagerFactory = Persistence.createEntityManagerFactory("clientes");
            }
            return entityManagerFactory.createEntityManager();
     }
}