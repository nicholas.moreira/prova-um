package br.unisul.aula.banco;

import java.util.*;

public interface CrudBanco<A> {

    void remove(Long id);
    void update(A a);
    void insert(A a);
    List<A> findAll();
    A findById(Long id);
}
