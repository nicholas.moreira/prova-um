package br.unisul.aula.banco;

import br.unisul.aula.modelo.Cliente;
import br.unisul.aula.modelo.UnidadeFederativa;
import br.unisul.aula.dtos.ClienteEnderecoRequest;

import javax.persistence.*;
import java.util.*;


public class FillCliente implements CrudBanco<Cliente>{

    @Override
    public void remove(Long id) {
        EntityManager entityManager = JPAUtil.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(Cliente.class, id));
        entityManager.getTransaction().commit();
    }


    @Override
    public void update(Cliente cliente) {
        EntityManager entityManager = JPAUtil.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(cliente);
        entityManager.getTransaction().commit();
    }

    @Override
    public void insert(Cliente cliente) {
        EntityManager entityManager = JPAUtil.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(cliente);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Cliente> findAll() {
        EntityManager entityManager = JPAUtil.getEntityManager();
        return entityManager
                .createQuery("SELECT e FROM Cliente e", Cliente.class)
                .getResultList();
    }

    @Override
    public Cliente findById(Long id) {
        EntityManager entityManager = JPAUtil.getEntityManager();
        TypedQuery<Cliente> query = entityManager
                .createQuery("SELECT s FROM Cliente s where s.id =:id", Cliente.class);
        return query.setParameter("id", id).getSingleResult();
    }

    public List<Cliente> findClientsByCity(ClienteEnderecoRequest req) {
        String cidade = req.getCidade();
        String uf = req.getUf();
        EntityManager entityManager = JPAUtil.getEntityManager();
        return entityManager
                .createQuery("SELECT s FROM Cliente s WHERE s.endereco.cidade = :cidade and s.endereco.uf = :uf", Cliente.class)
                .setParameter("cidade", cidade)
                .setParameter("uf", UnidadeFederativa.valueOf(uf))
                .getResultList();

    }







}
