package br.unisul.aula.servlet;

import br.unisul.aula.banco.FillCliente;
import br.unisul.aula.dtos.ClienteDTO;
import br.unisul.aula.dtos.ClienteEnderecoRequest;
import com.google.gson.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "EnderecoServletController", value = "/endereco")
public class EnderecoServletController extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        BufferedReader reader = request.getReader();
        ClienteEnderecoRequest req = new Gson().fromJson(reader, ClienteEnderecoRequest.class);
        List<ClienteDTO> clientes = new FillCliente()
                .findClientsByCity(req)
                .stream()
                .map(ClienteDTO::new)
                .collect(Collectors.toList());


        String episodioJson = new Gson().toJson(req.toRes(clientes));
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().println(episodioJson);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
