package br.unisul.aula.servlet;

import br.unisul.aula.banco.FillCliente;
import br.unisul.aula.dtos.ClienteDTO;
import com.google.gson.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet(name="ClienteServletController", value="/cliente")
public class ClienteServletController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<ClienteDTO> clientesDTO =  new FillCliente()
                .findAll()
                .stream()
                .map(ClienteDTO::new)
                .collect(Collectors.toList());

        String episodioJson = new Gson().toJson(clientesDTO);
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().println(episodioJson);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        ClienteDTO clienteDTO = new Gson().fromJson(reader, ClienteDTO.class);
        new FillCliente().remove(clienteDTO.getId());
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        new FillCliente().update(new Gson().fromJson(reader, ClienteDTO.class).vaiCliente());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        new FillCliente().insert(new Gson().fromJson(reader, ClienteDTO.class).vaiCliente());
    }


}
