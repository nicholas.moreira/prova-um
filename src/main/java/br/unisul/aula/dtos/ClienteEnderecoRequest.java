package br.unisul.aula.dtos;

import br.unisul.aula.modelo.Cliente;
import java.util.*;

public class ClienteEnderecoRequest {
    private String cidade;
    private  String uf;
     public ClienteEnderecoRequest() {

     }

    public ClienteEnderecoResponse toRes(List<ClienteDTO> clientes) {
        ClienteEnderecoResponse res = new ClienteEnderecoResponse();
        res.setUf(uf);
        res.setCidade(cidade);
        res.setClientes(clientes);
        return res;
    }

    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        cidade = cidade;
    }
    public String getUf() {
        return uf;
    }
    public void setUf(String uf) {
        this.uf = uf;
    }

}
