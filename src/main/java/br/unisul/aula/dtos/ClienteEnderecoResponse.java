package br.unisul.aula.dtos;

import java.util.*;
import java.util.stream.Collectors;

public class ClienteEnderecoResponse {
    private String cidade;
    private String uf;
    private List<ClienteDTO> clientes;

    public ClienteEnderecoResponse () {

    }

    public void setClientes(List<ClienteDTO> clientes) {
        this.clientes = clientes.stream().map(ClienteDTO::primeiro).collect(Collectors.toList());
    }

    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getUf() {
        return uf;
    }
    public void setUf(String uf) {
        this.uf = uf;
    }
    public List<ClienteDTO> getClientes() {
        return clientes;
    }

}
